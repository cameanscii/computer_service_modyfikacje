package com.javagda14.computerservice.service;



import com.javagda14.computerservice.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private ClientService clientService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Client> clientOptional = clientService.findByEmail(email);
        if (clientOptional.isPresent()) {
            // poprawne logowanie
            Client client = clientOptional.get();

            return new User(client.getEmail(), client.getPassword(), createAuthorities(client.getEmail()));
        }
        // brak użytkownika z podanym mailem!
        return null;
    }

    private Collection<? extends GrantedAuthority> createAuthorities(String email) {
        Set<GrantedAuthority> authoritySet = new HashSet<>();

        // taki hack. Robimy uprawnienia admin'a dla użytkownika o loginie admin@admin.admin
        if (email.equals("admin@admin.admin")) {
            authoritySet.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

            // ROLE_ <- wymagany początek nazwy roli
        }

        // każdy użytkownik otrzymuje uprawnienia USER
        authoritySet.add(new SimpleGrantedAuthority("ROLE_USER"));

        // zwracamy kolekcję
        return authoritySet;
    }

    public Optional<Client> getLoggedInUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            // nie jesteśmy zalogowani
            return Optional.empty();
        }

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return clientService.findByEmail(user.getUsername());
            // jesteśmy zalogowani, zwracamy user'a
        }

        return Optional.empty();
    }
}
