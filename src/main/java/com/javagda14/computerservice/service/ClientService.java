package com.javagda14.computerservice.service;


import com.javagda14.computerservice.model.Client;
import com.javagda14.computerservice.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Optional<Client> findByEmail(String email) {
        return clientRepository.findByEmail(email);
    }

}

