package com.javagda14.computerservice.repository;

import com.javagda14.computerservice.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    List<Client> findAllByNameContainingOrEmailContainingOrSurnameContaining(String name, String email, String surname);

    Optional<Client> findByEmail(String email);
}
