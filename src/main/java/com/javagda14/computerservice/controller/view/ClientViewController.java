package com.javagda14.computerservice.controller.view;

import com.javagda14.computerservice.service.ClientService;
import com.javagda14.computerservice.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/view/client/")
public class ClientViewController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private LoginService loginService;

//    private int licznik = 0;
//    @GetMapping("/test")
    // wartość zwracana to nazwa pliku bez rozszerzenia html w katalogu templates
//    public String getAddView(Model model) {
    // model możemy przekazać w parametrze, ale jest opcjonalny
//        model.addAttribute("licznik", licznik);
    // attribute to to samo co attribute w request (JSP)
//        return "index";
//    }



}
